<?php

namespace Lukaspotthast\kasapi;

use SoapClient;
use SoapFault;

/**
 * Class Kas_Api
 * @package Lukaspotthast\kasapi
 */
class Kas_Api
{

    const KAS_AUTH_WSDL = 'https://kasapi.kasserver.com/soap/wsdl/KasAuth.wsdl';
    const KAS_API_WSDL = 'https://kasapi.kasserver.com/soap/wsdl/KasApi.wsdl';

    const LOG_LEVEL_INFO = 3;
    const LOG_LEVEL_WARNING = 4;
    const LOG_LEVEL_ERROR = 5;

    const INFO = 'info';
    const WARNING = 'warning';
    const ERROR = 'error';

    /**
     * KAS username which will be used to log into the system.
     * For example: 'w0213456'
     * @var ?string
     *      The initial null value leads to an exception if the user tries to invoke an api function without
     *      providing his credentials.
     */
    private $kas_user = null;

    /**
     * KAS password which will be used to log into the system.
     * @var ?string
     *      The initial null value leads to an exception if the user tries to invoke an api function without
     *      providing his credentials.
     */
    private $kas_password = null;

    /**
     * Token lifetime in seconds until a new authentication is required.
     * @var int
     */
    private $session_lifetime = 60;

    /**
     * true: Calling api functions will reset the session lifetime. <br>
     * false: Calling api functions will not reset the session lifetime.
     *        The session expires after $session_lifetime seconds.
     * @var bool
     */
    private $session_update_lifetime = true;

    /**
     * Session token which will be used in every KAS request to identify us.
     * Gets set by the authenticate() function.
     * @var string
     */
    private $credential_token = null;

    /**
     * Stores the timestamp of the last authentication. Helps to determine if the $credential_token might be expired.
     * @var int Unix timestamp.
     */
    private $last_authentication_time = 0;

    /**
     * Stores the timestamp of the last request. Helps to determine if the $credential_token might be expired.
     * @var int Unix timestamp.
     */
    private $last_request_time = 0;

    /**
     * Stores an array for each api function name, containing a timestamp representing the last time the api function
     * got requested as well as the flood delay value which got provided by that last request.
     * Used to determine whether or not an api function can be called again.
     * @var array
     */
    private $flood_delay_information = [];

    /**
     * This callable gets called to log data. It receives a log type (string) and a message (string).
     * Will not be used if null.
     * @var ?callable
     */
    private $logger_func = null;

    /**
     * Determines which messages will get logged. For example: LOG_LEVEL_WARNING means that warnings and more critical
     * messages like errors will be logged.
     * @var bool
     */
    private $log_level = self::LOG_LEVEL_WARNING;


    /**
     * Kas_Api constructor.
     * Immediately sets the user credentials if both $kas_user and $kas_password get specified.
     *
     * @param null|string $kas_user
     *      Your KAS username. For example: w0213456
     * @param null|string $kas_password
     *      Your KAS password as a raw string.
     */
    public function __construct(?string $kas_user = null, ?string $kas_password = null)
    {
        if ( $kas_user !== null and $kas_password !== null )
        {
            $this->set_user_credentials($kas_user, $kas_password);
        }
    }



    /* --------------------------------------------------------------------------------------------------------------*/

    /* ACCOUNT */

    /**
     * Retrieves the information about either all available accounts or a specific account specified
     * by $account_login. <br>
     *
     * @param string $account_login
     *      The name / login of a specific account or null to obtain the information of all accounts.
     *
     * @return array
     *      Array of arrays. Each array holds information about a single account.
     *
     * @throws Kas_Exception
     */
    public function get_accounts(?string $account_login = null): array
    {
        $request_params = $account_login === null ? [] : ['account_login' => $account_login];
        $request = $this->call('get_accounts', $request_params);
        return $request['Response']['ReturnInfo'];
    }

    /**
     * @return array
     */
    public function get_accountressources(): array
    {
        $request = $this->call('get_accountressources', []);
        return $request['Response']['ReturnInfo'];
    }

    public function get_accountsettings(): array
    {
        $request = $this->call('get_accountsettings', []);
        return $request['Response']['ReturnInfo'];
    }

    public function add_account(): void
    {
    }

    public function delete_account(): void
    {
    }

    public function update_account(): void
    {
    }

    public function update_accountsettings(): void
    {
    }

    public function get_server_information(): void
    {
    }

    public function update_superusersettings(): void
    {
    }



    /* CHOWN */

    /* CRONJOB */

    /* DATABASE */

    /**
     * Retrieves the information about either all available databases or a specific database specified
     * by $database_login (which is equivalent to the name of the database).
     *
     * @param string $database_login
     *      The name / login of a specific database or null to obtain the information of all databases.
     *
     * @return array
     *      Array of arrays. Each array holds information about a database.
     *
     * @throws Kas_Exception
     */
    public function get_databases(?string $database_login = null): array
    {
        $request_params = $database_login === null ? [] : ['database_login' => $database_login];
        $request = $this->call('get_databases', $request_params);
        return $request['Response']['ReturnInfo'];
    }

    /**
     * Creates a new database. The new name / login for the database gets set by the system and will be returned by
     * this function.
     *
     * @param string $database_password
     *      The password for the new database.
     * @param string $database_comment
     *      A comment for the new database.
     * @param string $database_allowed_hosts
     *      Hosts which are allowed to access this database.
     *
     * @return string
     *      Returns the name of the newly created database.
     *
     * @throws Kas_Exception
     */
    public function add_database(
        string $database_password,
        string $database_comment,
        string $database_allowed_hosts = ''
    ): string
    {
        $request_params = [
            'database_password' => $database_password,
            'database_comment' => $database_comment,
            'database_allowed_hosts' => $database_allowed_hosts
        ];
        $request = $this->call('add_database', $request_params);
        return $request['Response']['ReturnInfo'];
    }

    /**
     * Deletes the database with the specified name / login.
     *
     * @param string $database_login
     *      The database to delete.
     *
     * @throws Kas_Exception
     */
    public function delete_database(string $database_login): void
    {
        $request_params = [
            'database_login' => $database_login
        ];
        $this->call('delete_database', $request_params);
    }

    /**
     * Deletes the database with the specified name / login. <br>
     * Possible KAS exceptions are:
     * <pre>
     *      - nothing_to_do                                  // Nothing changed.
     *      - cant_connect_to_mysql_on_this_server           //
     *      - database_comment_syntax_incorrect              //
     *      - database_login_not_found                       //
     *      - database_allowed_hosts_syntax_incorrect        //
     *      - no_mysql_on_this_server                        //
     *      - password_syntax_incorrect                      //
     * </pre>
     *
     * @param string $database_login         The database to update.
     * @param null|string $new_password      OPTIONAL: A new password.
     * @param null|string $new_comment       OPTIONAL: A new comment.
     * @param null|string $new_allowed_hosts OPTIONAL: New allowed hosts string.
     */
    public function update_database(
        string $database_login,
        ?string $new_password,
        ?string $new_comment,
        ?string $new_allowed_hosts
    ): void
    {
        $request_params = ['database_login' => $database_login];
        if ($new_password !== null)
            $request_params['new_password'] = $new_password;
        if ($new_comment !== null)
            $request_params['new_comment'] = $new_comment;
        if ($new_allowed_hosts !== null)
            $request_params['new_allowed_hosts'] = $new_allowed_hosts;

        $this->call('update_database', $request_params);
    }


    /* DDNS */


    /* DIRECTORYPROTECTION */


    /* DNS */


    /* DOMAIN */


    /* FTPUSER */


    /* MAILACCOUNT */


    /* MAILFILTER */


    /* MAILFORWARD */


    /* MAILINGLIST */


    /* SAMBAUSER */


    /* SESSION */


    /* SOFTWAREINSTALL */


    /* SSL */


    /* STATISTIC */


    /* SUBDOMAIN */

    /**
     * Creates a new subdomain. <br>
     * Possible KAS exceptions are:
     * <pre>
     *      - account_is_dummyaccount                        //
     *      - domain_for_this_subdomain_doesnt_exist         //
     *      - domain_syntax_incorrect                        //
     *      - max_subdomain_reached                          //
     *      - couldnt_get_kas_resources                      //
     *      - redirect_status_syntax_incorrect               //
     *      - statistic_syntax_incorrect                     //
     *      - subdomain_exist_as_subdomain                   //
     *      - subdomain_path_syntax_incorrect                //
     *      - subdomain_syntax_incorrect                     //
     *      - wildcardsubdomain_not_in_contract              //
     *      - php_version_syntax_incorrect                   //
     *      - php_version_not_available_on_server            //
     * </pre>
     *
     * @param string $subdomain_name
     *      The name for the new subdomain. If you need 'foo.bar.com', set this parameter to 'foo' and the $domain_name
     *      (next) parameter to 'bar.com'. <br>
     *      INFO: In all other subdomain-related functions $subdomain_name refers to the combined name of the subdomain
     *      prefix and the main domain name like: 'foo.bar.com'!
     * @param string $domain_name
     *      The name of the domain for which the subdomain should be created. If you need 'foo.bar.com', set this
     *      parameter to 'bar.com' and the $subdomain_name (previous) parameter to 'foo'.
     * @param string $subdomain_path
     *      OPTIONAL: (recommended to set!) The (relative) path for the subdomains data in your webspace.
     *      The location in which your index.php / index.html file will reside.
     *      Start and end the path with a slash! For example: '/foo/bar/mySite/public/'. Defaults to: '/'
     * @param string $php_version
     *      OPTIONAL: The PHP version for this subdomain. Pattern: 'major.minor'. Defaults to: '7.0'
     * @param int $redirect_status
     *      OPTIONAL: One of 0, 301, 302, 307. Defaults to: 0
     * @param int $statistic_version
     *      OPTIONAL: The version of the statistics program to use.  One of 0, 4, 5, 7. Defaults to: 5
     * @param string $statistic_language
     *      OPTIONAL: The language in which the statistic get created. One of: 'de', 'en'. Defaults to: 'de'
     *
     * @throws Kas_Exception
     */
    public function add_subdomain(
        string $subdomain_name,
        string $domain_name,
        string $subdomain_path = '/',
        string $php_version = '7.0',
        int $redirect_status = 0,
        int $statistic_version = 5,
        string $statistic_language = 'de'
    ): void
    {
        $request_params = [
            'subdomain_name'     => $subdomain_name,
            'domain_name'        => $domain_name,
            'subdomain_path'     => $subdomain_path,
            'redirect_status'    => $redirect_status,
            'statistic_version'  => $statistic_version,
            'statistic_language' => $statistic_language,
            'php_version'        => $php_version,
        ];
        $this->call('add_subdomain', $request_params);
    }

    /**
     * Deletes the specified subdomain. <br>
     * Possible KAS exceptions are:
     * <pre>
     *      - subdomain_doesnt_exist     //
     *      - in_progress                // The KAS object is currently busy.
     *      - host_is_dummyhost          //
     * </pre>
     *
     * @param string $subdomain_name
     *
     * @throws Kas_Exception
     */
    public function delete_subdomain(string $subdomain_name): void
    {
        $this->call('delete_subdomain', ['subdomain_name' => $subdomain_name]);
    }

    /**
     * Retrieves the information for either all subdomains, or a specific subdomain specified by $subdomain_name. <br>
     *
     * @param null|string $subdomain_name A specific subdomain to retrieve information for or null. Defaults to: null.
     *
     * @return array Array of arrays. Each internal array represents a subdomain.
     *
     * @throws Kas_Exception
     */
    public function get_subdomains(?string $subdomain_name = null): array
    {
        $request_params = $subdomain_name === null ? [] : ['subdomain_name' => $subdomain_name];
        $request = $this->call('get_subdomains', $request_params);
        return $request['Response']['ReturnInfo'];
    }

    /**
     * Moves the specified subdomain from source_account to target_account. <br>
     * Possible KAS exceptions are:
     * <pre>
     *      in_progress	                                               // The KAS object is currently busy.
     *      nothing_to_do	                                           // Nothing changed.
     *      kas_login_syntax_incorrect                                 //
     *      target_is_equal_to_source                                  //
     *      host_is_dummyhost                                          //
     *      account_doesnt_belong_to_you                               //
     *      subdomain_not_found_in_kas                                 //
     *      no_valid_parent_domain_there                               //
     *      target_is_dummyaccount                                     //
     *      subdomain_has_active_fpse                                  //
     *      max_subdomain_for_subaccount_gt_change_value               //
     *      max_mail_account_for_subaccount_gt_change_value            //
     *      max_mail_forward_for_subaccount_gt_change_value            //
     *      max_mailinglist_for_subaccount_gt_change_value             //
     * </pre>
     *
     * @param string $subdomain_name The name of the subdomain which should get moved.
     * @param string $source_account The source (account in which the subdomain currently resides).
     * @param string $target_account The target (account to which the subdomain should be moved).
     *
     * @throws Kas_Exception
     */
    public function move_subdomain(string $subdomain_name, string $source_account, string $target_account): void
    {
        $request_params = [
            'subdomain_name' => $subdomain_name,
            'source_account' => $source_account,
            'target_account' => $target_account
        ];
        $this->call('add_subdomain', $request_params);
    }

    /**
     * Updates the settings of the specified subdomain. <br>
     * Possible KAS exceptions are:
     * <pre>
     *      in_progress	                                               // The KAS object is currently busy.
     *      nothing_to_do	                                           // Nothing changed.
     *      redirect_status_syntax_incorrect                           //
     *      statistic_syntax_incorrect	                               //
     *      subdomain_doesnt_exist	                                   //
     *      subdomain_has_active_fpse	                               //
     *      subdomain_path_syntax_incorrect	                           //
     *      php_version_syntax_incorrect	                           //
     *      php_version_not_available_on_server	                       //
     *      is_active_syntax_incorrect                                 //
     * </pre>
     *
     * @param string $subdomain_name
     * @param string $subdomain_path
     * @param int $redirect_status
     * @param int $statistic_version
     * @param string $statistic_language
     * @param string $php_version
     * @param bool $is_active Should this subdomain be active?
     */
    public function update_subdomain(
        string $subdomain_name,
        string $subdomain_path = '/',
        int $redirect_status = 0,
        int $statistic_version = 5,
        string $statistic_language = 'de',
        string $php_version = '7.0',
        bool $is_active = true
    ): void
    {
        $request_params = [
            'subdomain_name' => $subdomain_name,
            'subdomain_path' => $subdomain_path,
            'redirect_status' => $redirect_status,
            'statistic_version' => $statistic_version,
            'statistic_language' => $statistic_language,
            'php_version' => $php_version,
            'is_active' => $is_active
        ];
        $this->call('update_subdomain', $request_params);
    }


    /* SYMLINK */

    /**
     * Adds a symlink. <br>
     * Possible KAS exceptions:
     * <pre>
     *      - in_progress
     * </pre>
     *
     * @param string $symlink_target
     * @param string $symlink_name
     */
    public function add_symlink(string $symlink_target, string $symlink_name): void
    {
        $request_params = [
            'symlink_target' => $symlink_target,
            'symlink_name' => $symlink_name
        ];
        $this->call('add_symlink', $request_params);
    }

    /* --------------------------------------------------------------------------------------------------------------*/



    /**
     * Sends an authentication request to the server.
     */
    public function authenticate(): void
    {
        try
        {
            $soap_logon = new SoapClient(self::KAS_AUTH_WSDL);

            /** @noinspection PhpUndefinedMethodInspection */
            $credential_token = $soap_logon->KasAuth(json_encode([
                'KasUser'               => $this->kas_user,
                'KasAuthType'           => 'sha1',
                'KasPassword'           => sha1($this->kas_password),
                'SessionLifeTime'       => $this->session_lifetime,
                'SessionUpdateLifeTime' => $this->session_update_lifetime ? 'Y' : 'N'
            ]));

            $this->credential_token = $credential_token;
            $this->last_authentication_time = time();
        }
        catch (SoapFault $fault)
        {
            $this->handle_soap_fault($fault);
        }
    }

    /**
     * Checks if the user is authenticated on the KAS server. <br>
     * If this function returns false, the authenticate() method should be invoked. <br>
     * This function DOES NOT ASK the server if the authentication is still valid. It just predicts it based on local
     * variables. <br>
     * The call() function will therefore still check if authentication errors occurred and react to them, so that
     * the user gets a error-free usage experience: <br>
     * Initialize this object once, then issue api call whenever you want. This wrapper will always ensure proper
     * authentication.
     *
     * @return bool
     *      true if authenticated, false otherwise. May not be correct.
     */
    public function is_authenticated(): bool
    {
        $credential_token_set = $this->credential_token !== null;

        // The LAST REQUEST TIME determines if the credentials are still valid.
        if ( $this->session_update_lifetime === true )
        {
            $credential_token_expired = $this->last_request_time + $this->session_lifetime < time();
        }
        //The LAST AUTHENTICATION TIME determines if the credentials are still valid.
        else
        {
            $credential_token_expired = $this->last_authentication_time + $this->session_lifetime < time();
        }

        // The credential token must be set and must not be expired.
        return $credential_token_set and !$credential_token_expired;
    }


    private function before_request(): void
    {
        // Check if the API user set their credentials.
        if ( !$this->check_user_credentials() )
        {
            $this->error('no_credentials_set', 'User credentials must be set to use the API!');
        }

        // Check if the credentials token is set. If not -> authenticate.
        if ( !$this->is_authenticated() )
        {
            $this->authenticate();
        }
    }

    private function call(string $api_function_name, array $params): array
    {
        $this->before_request();
        $request = null;

        try
        {
            $request = $this->_call($api_function_name, $params);
        }
        catch (Kas_Exception $e)
        {
            // (Re-)authenticate and try again if the exception indicates that no session exists.
            // This will trigger a new authentication if the session token lifetime expired.
            if ( in_array($e->get_kas_error(), ['got_no_login_data', 'kas_session_invalid']) )
            {
                $this->authenticate();
                $request = $this->_call($api_function_name, $params);
            }
            // Any other exception will be rethrown.
            else
            {
                throw $e;
            }
        }

        return $request;
    }

    /**
     * Calls the api function specified by $api_function_name with arguments specified by $params.
     *
     * @param string $api_function_name
     * @param array $params
     *
     * @return array
     *
     * @throws Kas_Exception
     */
    private function _call(string $api_function_name, array $params): array
    {
        $request = null;

        try
        {
            // The script may sleep until the flood delay for the specified api function has passed.
            $this->wait_for($api_function_name);

            $soap_request = new SoapClient(self::KAS_API_WSDL);

            /** @noinspection PhpUndefinedMethodInspection */
            $request = $soap_request->KasApi(json_encode(array(
                'KasUser' => $this->kas_user,
                'KasAuthType' => 'session',
                'KasAuthData' => $this->credential_token,
                'KasRequestType' => $api_function_name,
                'KasRequestParams' => $params
            )));

            $this->update_flood_delay_information(
                $request['Request']['KasRequestType'],
                $request['Response']['KasFloodDelay']
            );
            $this->last_request_time = time();
        }
        catch (SoapFault $fault)
        {
            // Even if the api call fail, the server might still set a flood delay for the api function we tried.
            // We therefore add a default flood_delay, so that a retry will not lead into a flood delay error.
            $this->update_flood_delay_information($api_function_name,  0.5);
            $this->last_request_time = time();

            $this->handle_soap_fault($fault);
        }

        return $request;
    }


    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Checks if the specified api function is currently requestable (possible flood delay form a previous call
     * is reached).
     *
     * @param string $api_function_name The api function name to check.
     * @return bool true / false
     */
    private function is_requestable(string $api_function_name): bool
    {
        // If no flood information is available, the api function never got called before. => Is callable.
        if ( !array_key_exists($api_function_name, $this->flood_delay_information) )
        {
            return true;
        }

        $last_request = $this->flood_delay_information[$api_function_name]['request_time'];
        $flood_delay = $this->flood_delay_information[$api_function_name]['flood_delay'];
        $requestable_at = $last_request + $flood_delay;
        $current_time = microtime(true);

        return $current_time > $requestable_at;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * Calculates and returns the number of seconds until the specified api function can be called again.
     *
     * @param string $api_function_name The api function name to check.
     * @return float Time in seconds. Guaranteed to be positive (including 0).
     */
    private function requestable_in(string $api_function_name): float
    {
        // If no flood information is available, the api function never got called before. => Is directly callable.
        if ( !array_key_exists($api_function_name, $this->flood_delay_information) )
        {
            return 0.0;
        }

        $last_request = $this->flood_delay_information[$api_function_name]['request_time'];
        $flood_delay = $this->flood_delay_information[$api_function_name]['flood_delay'];
        $requestable_at = $last_request + $flood_delay;
        $requestable_in = $requestable_at - microtime(true);

        return $requestable_in < 0 ? 0 : $requestable_in;
    }

    private function update_flood_delay_information(string $api_function_name, float $new_flood_delay): void
    {
        $this->flood_delay_information[$api_function_name] = [
            // Only works with our current server time!
            // Not the timestamp provided back through the $request['Request']['KasRequestTime'] value.
            'request_time' => microtime(true),
            'flood_delay' => $new_flood_delay
        ];
    }

    /**
     * Lets the function sleep until the specified api function can be requested again.
     *
     * @param string $api_function_name The api function to wait for.
     */
    private function wait_for(string $api_function_name): void
    {
        usleep($this->requestable_in($api_function_name) * 1000000);
    }

    /**
     * Generates and throws an KAS Exception from the incoming SoapFault object.
     *
     * @param SoapFault $fault
     *
     * @throws Kas_Exception
     */
    private function handle_soap_fault(SoapFault $fault): void
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $error_msg =
            'SoapFault:' . PHP_EOL
            .'code: '    . $fault->getCode()          . PHP_EOL
            .'message: ' . $fault->getMessage()       . PHP_EOL
            .'in file: ' . $fault->getFile()          . PHP_EOL
            .'at line: ' . $fault->getLine()          . PHP_EOL
            //.'trace: '   . $fault->getTraceAsString() . PHP_EOL
        ;

        $this->error($fault->getMessage(), $error_msg);
    }

    /**
     * Throws an Kas_Exception and additionally logs the error message if possible and allowed.
     *
     * @param string $error_code
     * @param string $error_msg The message to print.
     *
     * @throws Kas_Exception
     */
    private function error(string $error_code, string $error_msg): void
    {
        if ( $this->should_log(self::LOG_LEVEL_ERROR) )
        {
            $this->log(self::ERROR, $error_msg);
        }

        throw new Kas_Exception($error_code, __CLASS__.': '.$error_msg);
    }

    /**
     * Logs the specified message.
     *
     * @param string $type
     * @param string $message
     */
    private function log(string $type, string $message): void
    {
        // We can only log something if a logger is defined.
        if ( isset($this->logger_func) )
        {
            call_user_func_array($this->logger_func, ['type' => $type, 'message' => $message]);
        }
    }

    /**
     * Checks if the specified LOG_LEVEL_* should be logged.
     * @param int $log_level One of the LOG_LEVEL_* constants of this class.
     * @return bool true / false
     */
    private function should_log(int $log_level): bool
    {
        return $this->log_level <= $log_level;
    }


    /**
     * @param callable $function
     */
    public function set_logger(callable $function): void
    {
        $this->logger_func = $function;
    }

    /**
     * Sets the user credentials.
     *
     * @param null|string $kas_user     Your KAS username. For example: w0213456
     * @param null|string $kas_password Your KAS password as a raw string.
     */
    public function set_user_credentials(string $kas_user, string $kas_password): void
    {
        $this->kas_user = $kas_user;
        $this->kas_password = $kas_password;

        // The old credential_token belonged to the old user credentials.
        $this->credential_token = null;
    }

    /**
     * Checks if the user credentials are set.
     *
     * @return bool true / false
     */
    public function check_user_credentials(): bool
    {
        return $this->kas_user !== null and $this->kas_password !== null;
    }

}