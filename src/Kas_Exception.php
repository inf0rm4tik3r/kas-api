<?php

namespace Lukaspotthast\kasapi;

use Exception;
use Throwable;

class Kas_Exception extends Exception
{

    /** @var string */
    private $kas_error;

    public function __construct($kas_code, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->kas_error = $kas_code;

        parent::__construct($message, $code, $previous);
    }

    public function get_kas_error(): string
    {
        return $this->kas_error;
    }

    public function get_error_message(): string
    {
        return $this->message;
    }

}