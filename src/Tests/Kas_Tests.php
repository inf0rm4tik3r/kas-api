<?php

namespace Lukaspotthast\kasapi\Tests;

use Lukaspotthast\kasapi\Kas_Api;
use Lukaspotthast\kasapi\Kas_Exception;

class Kas_Tests
{

    private $kas;

    public function __construct()
    {
        $this->kas = new Kas_Api('w016e05b', '9cpstAnDQsc4');
    }

    public function run_all(): array
    {
        return [
            'database' => $this->run_database_tests(),
            'subdomain' => $this->run_subdomain_tests()
        ];
    }


    public function run_database_tests(): array
    {
        $results = [
            'get_databases' => false,
            'add_database' => false,
            'delete_database' => false,
            'exceptions' => []
        ];

        try
        {
            $initial = $this->kas->get_databases();
            $results['get_databases'] = 'ok';

            $db_name = $this->kas->add_database('super_secure', 'foobar');
            $results['add_database'] = 'ok';

            $after_add = $this->kas->get_databases();

            $this->kas->delete_database($db_name);
            $results['delete_database'] = 'ok';

            $after_remove = $this->kas->get_databases();
        }
        catch (Kas_Exception $e)
        {
            array_push($results['exceptions'], $e);
        }

        return $results;
    }


    public function run_subdomain_tests(): array
    {
        return [];
    }

}